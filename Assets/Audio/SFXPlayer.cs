﻿using UnityEngine;

namespace KBKN.Sounds
{
    [RequireComponent(typeof(AudioSource))]
    public class SFXPlayer : MonoBehaviour
    {
        public static SFXPlayer Instance { get; private set; }





        public AudioClip ButtonClick;
        public AudioClip LevelComplete;
        public AudioClip PickJigsaw;
        public AudioClip InsertJigsaw;
        public AudioClip ReturnJigsaw;

        private AudioSource audioPlayer;




        
        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }

        private void Start()
        {
            audioPlayer = GetComponent<AudioSource>();
        }





        public void PlaySound(AudioClip clip)
        {
            audioPlayer.clip = clip;
            audioPlayer.Play();
        }

        public void PlayButtonClickSfx()
        {
            PlaySound(ButtonClick);
        }

        public void PlayLevelCompleteSfx()
        {
            PlaySound(LevelComplete);
        }

        public void PlayPickJigsawSfx()
        {
            PlaySound(PickJigsaw);
        }

        public void PlayInsertJigsawSfx()
        {
            PlaySound(InsertJigsaw);
        }

        public void PlayReturnJigsawSfx()
        {
            PlaySound(ReturnJigsaw);
        }
    }
}
