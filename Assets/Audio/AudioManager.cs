﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
public class AudioManager : MonoBehaviour
{
    public AudioClip[] audioClip;
    public AudioSource audioSource;
    public bool shuffle;
    private int nextClipIndex;
    private int? currentClipIndex;
    private bool isStopped;



    public bool IsStopped
    {
        get { return isStopped; }
        set
        {
            if (isStopped == value)
                return;

            if (value == true)
                StopPlaying();
            else
                StartPlaying();
        }
    }


    public void PlayNext()
    {

        if (currentClipIndex != null)
            audioClip[currentClipIndex.Value].UnloadAudioData();
        audioSource.clip = audioClip[nextClipIndex];
        currentClipIndex = nextClipIndex;
        nextClipIndex = GetNextClipIndex();
        audioClip[nextClipIndex].LoadAudioData();
        audioSource.Play();

    }
    private int GetNextClipIndex()
    {
        if(shuffle == true)
        {
            int nextClipIndex;
            do nextClipIndex = Random.Range(0, audioClip.Length);
            while (nextClipIndex == currentClipIndex);
            return nextClipIndex;
        }
        else
        {
            if (currentClipIndex == null)
                return 0;
            else
                return (int)((currentClipIndex + 1) % audioClip.Length);
        }
    }
    void Update()
    {
      if (audioSource.isPlaying == false && isStopped == false)
          PlayNext();
    }
    public void StopPlaying()
    {
        audioSource.Stop();
        isStopped = true;
    }
     public void PlaySound(int sound)
    {
        audioSource.clip = audioClip[sound];
        audioSource.Play();
    }
    public void StartPlaying()
    {
        isStopped = false;
    }
}
