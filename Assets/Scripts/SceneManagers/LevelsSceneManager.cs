﻿using UnityEngine.SceneManagement;
using UnityEngine;
using KBKN.Utilities;
using KBKN.Sounds;

namespace KBKN.SceneManagers
{
    public class LevelsSceneManager : MonoBehaviour
    {
        private const int SelectedChapter_None = -1;
        private const int SelectedLevel_None = -1;





        [SerializeField] private GameObject chaptersWindow;
        [SerializeField] private GameObject levelsWindow;
        [SerializeField] private GameObject levelInfoOverlay;
        [SerializeField] private ScreenFader screenFader;

        private int selectedChapter;
        private int selectedLevel;





        private void Start()
        {
            screenFader.FadeIn();

            chaptersWindow.SetActive(true);
            levelsWindow.SetActive(false);

            selectedChapter = SelectedChapter_None;
            selectedLevel = SelectedLevel_None;
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel") == true)
            {
                SFXPlayer.Instance.PlayButtonClickSfx();
                GoBack();
            }
        }





        public void DeselectCurrentChapter()
        {
            if (selectedChapter == SelectedChapter_None)
                return;

            selectedChapter = SelectedChapter_None;
            HideLevelsWindow();
        }

        public void DeselectCurrentLevel()
        {
            if (selectedLevel == SelectedLevel_None)
                return;

            selectedLevel = SelectedLevel_None;
            HideLevelInfoOverlay();
        }

        public void GoBack()
        {
            if (selectedLevel != SelectedLevel_None)
                DeselectCurrentLevel();
            else if (selectedChapter != SelectedChapter_None)
                DeselectCurrentChapter();
            else
                LoadStartMenu();
        }

        public void SelectChapter(int chapterIndex)
        {
            if (selectedChapter != SelectedChapter_None)
                return;


            selectedChapter = chapterIndex;

            LoadLevels(chapterIndex);
            ShowLevelsWindow();
        }

        public void SelectLevel(int levelIndex)
        {
            if (selectedChapter == SelectedChapter_None)
                return;

            selectedLevel = levelIndex;
            ShowLevelInfoOverlay(levelIndex);
        }

        public void StartSelectedLevel()
        {
            if (selectedLevel == SelectedLevel_None)
                return;
            if (screenFader.IsFading == true)
                return;


            screenFader.OnFadedOut += () => SceneManager.LoadScene("Game");
            screenFader.FadeOut();
        }





        private void LoadLevels(int chapterIndex)
        {
            Debug.LogFormat("Loading levels for chapter {0}", chapterIndex);
            Debug.LogWarning("Not implemented yet!");
        }

        private void LoadStartMenu()
        {
            if (screenFader.IsFading == true)
                return;

            screenFader.OnFadedOut += () => SceneManager.LoadScene("StartMenu");
            screenFader.FadeOut();
        }

        private void HideLevelInfoOverlay()
        {
            if (levelInfoOverlay.activeInHierarchy == false)
                return;

            levelInfoOverlay.SetActive(false);
        }

        private void HideLevelsWindow()
        {
            if (levelsWindow.activeInHierarchy == false)
                return;

            chaptersWindow.SetActive(true);
            levelsWindow.SetActive(false);
        }

        private void ShowLevelInfoOverlay(int levelIndex)
        {
            // TODO: uncomment code below
            //if (levelInfoOverlay.activeInHierarchy == true)
            //    return;
            if (levelIndex == SelectedLevel_None)
                return;

            Debug.LogWarning("Showing level info overlay not yet implemented!");
            Debug.Log("Starting selected level instead!");

            StartSelectedLevel();
        }

        private void ShowLevelsWindow()
        {
            if (levelsWindow.activeInHierarchy == true)
                return;

            chaptersWindow.SetActive(false);
            levelsWindow.SetActive(true);
        }
    }
}