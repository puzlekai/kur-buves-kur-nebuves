﻿using KBKN.Sounds;
using KBKN.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KBKN.SceneManagers
{
    public sealed class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }





        [SerializeField] private ScreenFader screenFader;
        [SerializeField] private GameObject gameWonOverlay;





        private void Awake()
        {
            if(Instance != null)
            {
                Debug.LogWarning("There's another instance of GameManager. Destroying it.");
                Destroy(Instance);
            }

            Instance = this;
        }

        private void Start()
        {
            gameWonOverlay.SetActive(false);
            screenFader.FadeIn();
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel") == true)
            {
                SFXPlayer.Instance.PlayButtonClickSfx();
                LoadLevelsScene();
            }
        }





        public void ShowGameWonOverlay()
        {
            if (gameWonOverlay.activeInHierarchy == true)
                return;

            SFXPlayer.Instance.PlayLevelCompleteSfx();
            gameWonOverlay.SetActive(true);
        }

        public void LoadLevelsScene()
        {
            if (screenFader.IsFading == true)
                return;

            screenFader.OnFadedOut += () => SceneManager.LoadScene("Levels");
            screenFader.FadeOut();
        }
    }
}
