﻿using KBKN.Sounds;
using KBKN.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace KBKN.SceneManagers
{
    public class StartMenuManager : MonoBehaviour
    {
        [SerializeField] private GameObject menuObject;
        [SerializeField] private GameObject settingsObject;

        [SerializeField] private ScreenFader screenFader;





        private void Start()
        {
            screenFader.FadeIn();
        }

        private void Update()
        {
            if(Input.GetButtonDown("Cancel") == true)
            {
                SFXPlayer.Instance.PlayButtonClickSfx();
                ExitGame();
            }
        }





        public void LoadLevels()
        {
            screenFader.OnFadedOut += () => SceneManager.LoadScene("Levels");
            screenFader.FadeOut();
        }

        public void ShowSettings()
        {
            if (settingsObject.activeInHierarchy == true)
                return;

            settingsObject.SetActive(true);
            menuObject.SetActive(false);
        }

        public void HideSettings()
        {
            if (settingsObject.activeInHierarchy == false)
                return;

            settingsObject.SetActive(false);
            menuObject.SetActive(true);
        }

        public void ExitGame()
        {
            screenFader.OnFadedOut += () => Application.Quit();
            screenFader.FadeOut();
        }
    }
}