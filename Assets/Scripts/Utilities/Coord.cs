﻿namespace KBKN.Utilities
{
   public struct Coord
    {
        public byte x;
        public byte y;




        public Coord(byte x, byte y)
        {
            this.x = x;
            this.y = y;
        }





        public override bool Equals(object obj)
        {
            if ((obj is Coord) == false)
                return false;

            var coord = (Coord)obj;
            return x == coord.x &&
                   y == coord.y;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return $"x = {x}; y = {y}";
        }





        public static bool operator ==(Coord a, Coord b)
        {
            return a.x == b.x && a.y == b.y;
        }
        public static bool operator !=(Coord a, Coord b)
        {
            return a.x != b.x || a.y != b.y;
        }
    }
}