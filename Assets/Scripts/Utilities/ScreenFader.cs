﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using System;


namespace KBKN.Utilities
{
    [RequireComponent(typeof(RawImage))]
    public class ScreenFader : MonoBehaviour
    {
        public event Action OnFadedIn;
        public event Action OnFadedOut;





        private RawImage overlay;
        private Coroutine fadeRoutine;





        public bool IsFading
        {
            get { return fadeRoutine != null; }
        }





        private void Awake()
        {
            overlay = GetComponent<RawImage>();
        }




        public void FadeIn(int perFrames = 30)
        {
            if (fadeRoutine != null)
                return;

            gameObject.SetActive(true);
            fadeRoutine = StartCoroutine(FadeInRoutine(perFrames));
        }

        public void FadeOut(int perFrames = 30)
        {
            if (fadeRoutine != null)
                return;

            gameObject.SetActive(true);
            fadeRoutine = StartCoroutine(FadeOutRoutine(perFrames));
        }





        private IEnumerator FadeInRoutine(int perFrames)
        {
            for (float i = 1; i <= perFrames; i++)
                yield return overlay.color = Color.LerpUnclamped(Color.black, Color.clear, i / perFrames);

            gameObject.SetActive(false);
            OnFadedIn?.Invoke();
            fadeRoutine = null;
        }

        private IEnumerator FadeOutRoutine(int perFrames)
        {
            for (float i = 1; i <= perFrames; i++)
                yield return overlay.color = Color.LerpUnclamped(Color.clear, Color.black, i / perFrames);

            OnFadedOut?.Invoke();
            fadeRoutine = null;
        }
    }
}