﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauze : MonoBehaviour {

    public static bool GameIsPoused = false;

    public GameObject pauseMenuUI;

    // Update is called once per frame
    public void Pause()
    {
        if (!GameIsPoused)
        {
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            GameIsPoused = true;
        }
    }

    public void Resume()
    {
        if (GameIsPoused)
        {
            pauseMenuUI.SetActive(false);
            Time.timeScale = 1f;
            GameIsPoused = false;
        }
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("PradinisMeniu");
        GameIsPoused = false;
    }

}

