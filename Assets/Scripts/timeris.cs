﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timeris : MonoBehaviour {

    public float timeToWait;
    private float currentWaitTime;
    private bool checkTime;
    public GameObject button;

    private void Start()
    {
        button.SetActive(false);
    }
    void Awake()
    {
        ResetTimer();
        button.SetActive(false);
    }
    void Update()
    {
        if (checkTime)
        {
            currentWaitTime -= Time.deltaTime;
            if (currentWaitTime < 0 || Input.GetMouseButtonDown(0))
            {
                TimerFinished();
                checkTime = false;
            }
        }
    }

    public void ResetTimer()
    {
        currentWaitTime = timeToWait;
        checkTime = true;
        button.SetActive(false);
    }
    void TimerFinished()
    {
        button.SetActive(true);
    }
}
