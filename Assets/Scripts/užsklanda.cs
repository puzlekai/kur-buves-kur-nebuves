﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class užsklanda : MonoBehaviour
{

    public GameObject uzsklanda;
    public float timeToWait = 2f;

    public static bool ArRodomaUžsklanda = false;


    // Update is called once per frame

    public void ijungtiužskl()
    {
        if (!ArRodomaUžsklanda)
        {
            uzsklanda.SetActive(true);
        }
        ArRodomaUžsklanda = true;
    }
}
