﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class laikas : MonoBehaviour {
    public float laikass;
    public Text laikotekstas;
    public GameObject Pralaimėjimas;
    // Use this for initialization
    Coroutine Laikmatis;
    // Update is called once per frame

    private void Start()
    {
        Laikmatis = null;
    }


    public void Pradeti(float laikass = 5)
    {
        if (Laikmatis == null)
        {
            this.laikass = laikass;
            Laikmatis = StartCoroutine(Laik());
       }

    }

    IEnumerator Laik()
    {
        while (laikass > 0)
        {
            laikotekstas.text = ("Liko laiko = " + laikass);
            yield return new WaitForSeconds(1);
            laikass--;           
        }
        Pralaimėjimas.SetActive(true);
        Laikmatis = null;
    }
    public void ijungtiužskl()
    {
            StopAllCoroutines();
            Laikmatis = null;
    }
}
