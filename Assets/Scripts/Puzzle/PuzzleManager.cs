﻿using KBKN.Puzzle;
using KBKN.SceneManagers;
using KBKN.Utilities;
using UnityEngine;

namespace KBKN.Puzzle
{
    public class PuzzleManager : MonoBehaviour
    {
        /// <summary>
        /// Dabar aktyvus PuzzleHolder objektas
        /// </summary>
        public static PuzzleManager Instance { get; private set; }


        private int unplaced;
        public static bool IsInputEnabled = true;
        PuzzleMaker make = new PuzzleMaker();






        /// <summary>
        /// Vykdomas, kai skritpas sukuriamas
        /// </summary>
        private void Awake()
        {
            if (Instance != null)
            {
                Debug.Log("Yra dar vienas aktyvus PuzzleManager objektas. Sunaikinu jį.");
                Destroy(Instance);
            }

            Instance = this;
        }

        public void Start()
        {
            make.source = (Texture2D) Resources.Load("texture_SampleTexture");
            make.Generate();
            unplaced = (int) PuzzleHolder.Instance.PiecesCount.x * (int) PuzzleHolder.Instance.PiecesCount.y;
        }




        /// <summary>
        /// Stebi deliones progresa
        /// </summary>
        public void ArIveiktasLygis()
        {
            unplaced--;

            if (unplaced == 0)
                GameManager.Instance.ShowGameWonOverlay();
        }
    }
}
