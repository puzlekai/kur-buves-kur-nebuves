﻿using UnityEngine;
using KBKN.Puzzle;
using KBKN.Utilities;
using KBKN.Sounds;

namespace KBKN.Puzzle
{
    [RequireComponent(typeof(PuzzlePiece))]
    public class PiecePlacement : MonoBehaviour
    {
        public enum PieceStatus { PickedUp, Idle, Placed };





        /// <summary>
        /// Detalė, kurią valdau
        /// </summary>
        private PuzzlePiece controlledPiece;
        private PieceStatus pieceStatus = PieceStatus.Idle;




        private void Start()
        {
            controlledPiece = GetComponent<PuzzlePiece>();
        }

        // Update is called once per frame
        private void Update()
        {
            if (PuzzleManager.IsInputEnabled)
            {
                // Jeigu detale paimta, ji valkiojama kartu su peles kursoriumi
                if (pieceStatus == PieceStatus.PickedUp)
                {
                    Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                    Vector2 objectPosition = Camera.main.ScreenToWorldPoint(mousePosition);
                    transform.position = objectPosition;
                }

                // Jei paspaudė dešinį pelės klavišą
                if (Input.GetMouseButtonDown(1))
                {
                    // Jei detalė paimta - bandyt ją padėt
                    if (pieceStatus == PieceStatus.PickedUp)
                    {
                        Coord? pieceCoord = PuzzleHolder.Instance.WorldPosToCoord(transform.position);

                        // Jeigu detalė virš lentelės ir tinkamoje pozicijoje
                        if (pieceCoord != null && PuzzleHolder.Instance.TryPlace(controlledPiece, pieceCoord) == true)
                        {
                            SFXPlayer.Instance.PlayInsertJigsawSfx();

                            PuzzleHolder.Instance.Place(controlledPiece);
                            pieceStatus = PieceStatus.Placed;
                            PuzzleManager.Instance.ArIveiktasLygis();
                        }
                        else
                        {
                            SFXPlayer.Instance.PlayReturnJigsawSfx();
                        }
                    }
                }
            }
        }

        /* Registruoja peles mygtuko paspaudima ant deliones dalies. 
         Pakelta delione kai valkiojama virs kitu deliones daliu visada yra virsuje ju. */
        void OnMouseDown()
        {
            if(pieceStatus == PieceStatus.Idle && PuzzleManager.IsInputEnabled == true)
            {
                SFXPlayer.Instance.PlayPickJigsawSfx();

                pieceStatus = PieceStatus.PickedUp;
                transform.localScale *= 1.25f;

                GetComponent<Renderer>().sortingOrder = 10;
            }
        }
    }
}