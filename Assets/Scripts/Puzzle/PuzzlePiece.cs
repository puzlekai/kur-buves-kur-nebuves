﻿using UnityEngine;
using System;
using KBKN.Utilities;

namespace KBKN.Puzzle
{
    [RequireComponent(typeof(MeshRenderer))]
    public sealed class PuzzlePiece : MonoBehaviour
    {
        /// <summary>
        /// Šablono vieta resūrsų aplankale
        /// </summary>
        private const string Location_Prefab = "prefab_PuzzlePiece";





        private Coord? coord;





        public Coord? Coord
        {
            get { return coord; }
            set
            {
                if (coord != null)
                    return;

                coord = value;
            }
        }





        private void Start()
        {
            if (coord == null)
                throw new Exception("Coord is not set by the time the script started");
        }





        public void SetTexture(Texture2D texture)
        {
            GetComponent<MeshRenderer>().material.SetTexture("_MainTex", texture);
        }

        public void SetUV(Vector4 uv)
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;

            Vector2[] uvs = mesh.uv;
            uvs[0] = new Vector2(uv.x, uv.y);
            uvs[3] = new Vector2(uv.x, uv.w);
            uvs[1] = new Vector2(uv.z, uv.w);
            uvs[2] = new Vector2(uv.z, uv.y);
            mesh.uv = uvs;
        }





        internal static PuzzlePiece GetPrefab()
        {
            return Resources.Load<PuzzlePiece>(Location_Prefab);
        }
        //internal static PuzzlePiece Generate(PuzzlePieceSideType[] sideTypes, Transform parent, PuzzlePiece prefab = null)
        //{
        //    if (sideTypes.Length != 4)
        //        throw new ArgumentException("invalid array length", nameof(sideTypes));

        //    if (prefab == null)
        //        prefab = GetPrefab();

        //    PuzzlePiece piece = Instantiate(prefab, parent);
        //    piece.sideTypes = sideTypes;
        //    return piece;
        //}
    }
}