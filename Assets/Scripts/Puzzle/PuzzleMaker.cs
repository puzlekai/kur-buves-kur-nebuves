﻿using KBKN.Utilities;
using UnityEngine;


namespace KBKN.Puzzle
{
    public sealed class PuzzleMaker
    {
        public Texture2D source;
        public byte piecesCountX;
        public byte piecesCountY;
        public Vector2 puzzleWorldSize;



        private void Start()
        {
            //Generator generator = new Generator
            //{
            //    holder = PuzzleHolder.Instance.transform,
            //    puzzleWorldSize = puzzleWorldSize,
            //    piecesCountX = piecesCountX,
            //    piecesCountY = piecesCountY,
            //    source = source
            //};

            //generator.Generate();
            //PuzzleHolder.Instance.PiecesCount = new Coord(piecesCountX, piecesCountY);
            //PuzzleHolder.Instance.Size = puzzleWorldSize;
        }

        public void Generate()
        {
            Generator generator = new Generator
            {
                holder = PuzzleHolder.Instance.transform,
                puzzleWorldSize = new Vector2(16, 9),
                piecesCountX = 4,
                piecesCountY = 3,
                source = source
            };

            generator.Generate();
            PuzzleHolder.Instance.PiecesCount = new Coord(generator.piecesCountX, generator.piecesCountY);
            PuzzleHolder.Instance.Size = generator.puzzleWorldSize;
        }
    }
}
