﻿using KBKN.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KBKN.Puzzle
{
    /// <summary>
    /// Puzlės detales laikantis objektas
    /// </summary>
    public sealed class PuzzleHolder : MonoBehaviour
    {
        /// <summary>
        /// Dabar aktyvus PuzzleHolder objektas
        /// </summary>
        public static PuzzleHolder Instance { get; private set; }





        /// <summary>
        /// Dalių kiekis dėlionėje
        /// </summary>
        private Coord piecesCount;
        /// <summary>
        /// Lentelės dydis (pasaulinėmis koordinatėmis)
        /// </summary>
        private Vector2 size;
        /// <summary>
        /// Laikančiojo objekto keturkampį apibrėžiantis objektas
        /// </summary>
        private Rect rect;
        /// <summary>
        /// Vienos dalies dydis (pasaulėnimis koordinatėmis)
        /// </summary>
        private Vector2 pieceSize;
        /// <summary>
        /// Netinkamoj vietoj padėtų detalių kiekis
        /// </summary>
        private int unplacedCount;





        public Coord PiecesCount
        {
            get { return piecesCount; }
            set
            {
                if (piecesCount == value)
                    return;

                piecesCount = value;
                UpdatePieceSize();
                UpdateRect();
            }
        }
        public Vector2 Size
        {
            get { return size; }
            set
            {
                if (size == value)
                    return;

                size = value;
                UpdatePieceSize();
                UpdateRect();
            }
        }





        /// <summary>
        /// Vykdomas, kai skritpas sukuriamas
        /// </summary>
        private void Awake()
        {
            if (Instance != null)
            {
                Debug.Log("Yra dar vienas aktyvus PuzzleHolder objektas. Sunaikinu jį.");
                Destroy(Instance);
            }

            Instance = this;
        }





        /// <summary>
        /// Paverčia dabartinę pelės vietą į koordinatę dėlionėje
        /// </summary>
        /// <returns>NULL, jei palė yra ne virš puzlę laikančio objekto</returns>
        public Coord? MousePosToCoord()
        {
            return WorldPosToCoord(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
        /// <summary>
        /// Tas pats kaip <see cref="MousePosToCoord"/>, tik kad pelės vieta nurodoma
        /// </summary>
        /// <param name="mousePos">Pelės vieta ekrano koordinatėmis</param>
        public Coord? MousePosToCoord(Vector2 mousePos)
        {
            return WorldPosToCoord(Camera.main.ScreenToWorldPoint(mousePos));
        }
        /// <summary>
        /// Pasaulinę koordinatę konvertuoja į lentelės koordinatę
        /// </summary>
        /// <returns>NULL, jei pasaulinė koordinatė nėra lentelės viduje</returns>
        public Coord? WorldPosToCoord(Vector2 worldPoint)
        {
            if (rect.Contains(worldPoint, true) == false)
                return null;

            byte coordX = (byte) ((worldPoint.x - rect.x) / pieceSize.x);
            byte coordY = (byte) ((worldPoint.y - rect.y) / pieceSize.y);

            return new Coord(coordX, coordY);
        }

        /// <summary>
        /// Gražina puzlės koordinatės centro pasaulinę poziciją
        /// </summary>
        /// <param name="coord">Puzlės koordinatė</param>
        /// <returns></returns>
        public Vector3 CoordToPosition(Coord coord)
        {
            float posX = rect.xMin + pieceSize.x * (coord.x + 0.5f);
            float posY = rect.yMin + pieceSize.y * (coord.y + 0.5f);

            return new Vector3(posX, posY, 0);
        }




        /// <summary>
        /// Padeda detalę ant puzlę laikančio objekto
        /// </summary>
        /// <param name="piece">Detalė, kurią reikia padėti</param>
        internal void Place(PuzzlePiece piece)
        {
            piece.transform.position = CoordToPosition(piece.Coord.Value);
            piece.GetComponent<Renderer>().sortingOrder = 0;
            piece.transform.localScale /= 1.25f;


            unplacedCount--;

            if (unplacedCount == 0)
                Debug.Log("Lygis įveiktas");
        }


        /// <summary>
        /// Tikrina ar detale savo vietoje
        /// </summary>
        /// <param name="piece">Detalė, kurią tikrina</param>
        /// <param name="coord">Detalės koordinatė lentelėje</param>
        public bool TryPlace(PuzzlePiece piece, Coord? coord)
        {
           if (piece.Coord == coord)
           {
               return true;
           }
           else
           {
               return false;
           }
       }


        /// <summary>
        /// Atnaujina dalies dydį (pasaulinėmis koordinatėmis) pagal dabartinius duomenis
        /// </summary>
        private void UpdatePieceSize()
        {
            pieceSize = new Vector2(size.x / piecesCount.x, size.y / piecesCount.y);
        }
        /// <summary>
        /// Atnaujina laikančio objekto keturkampį (pasaulinėmis koordinatėmis) nusakantį objektą
        /// </summary>
        private void UpdateRect()
        {
            Vector2 center = transform.position;
            Vector2 maxOffset = size / 2;

            rect = new Rect()
            {
                max = center + maxOffset,
                min = center - maxOffset
            };
        }
    }
}
