﻿using KBKN.Utilities;
using UnityEngine;


namespace KBKN.Puzzle
{
    public sealed class Generator
    {
        private const float ScreenLimitX = 15;
        private const float ScreenLimitY = 8;

        public Transform holder;
        public Vector2 puzzleWorldSize;
        public byte piecesCountX;
        public byte piecesCountY;
        public Texture2D source;
        




        public PuzzlePiece[,] Generate(PuzzlePiece piecePrefab = null)
        {
            if (piecePrefab == null)
                piecePrefab = PuzzlePiece.GetPrefab();


            Vector3 pieceScale = new Vector3(puzzleWorldSize.x / piecesCountX, puzzleWorldSize.y / piecesCountY, 1);
            Vector2 startingPos = GetStartingPos(pieceScale);
            Vector2 currentPos = startingPos;
            float uvHeight = 1f / piecesCountY;
            float uvWidth = 1f / piecesCountX;
            

            PuzzlePiece[,] pieces = new PuzzlePiece[piecesCountX, piecesCountY];
            for (byte y = 0; y < piecesCountY; y++)
            {
                currentPos.x = startingPos.x;

                for (byte x = 0; x < piecesCountX; x++)
                {
                    pieces[x, y] = Object.Instantiate(piecePrefab, holder);
                    pieces[x, y].GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_MainTex", source);

                    Vector4 uvs = new Vector4();
                    uvs.x = uvWidth * x;
                    uvs.y = uvHeight * y;
                    uvs.z = uvs.x + uvWidth;
                    uvs.w = uvs.y + uvHeight;

                    pieces[x, y].Coord = new Coord(x, y);
                    pieces[x, y].SetUV(uvs);
                    pieces[x, y].transform.localScale = pieceScale;
                    pieces[x, y].transform.position = GetRandomInScreenPosition();

                    currentPos.x += pieceScale.x;
                }
                currentPos.y += pieceScale.y;
            }

            return pieces;
        }

        

        private Vector2 GetStartingPos(Vector2 pieceSize)
        {
            float startingX;
            if (piecesCountX % 2 == 0)
                startingX = pieceSize.x * (piecesCountX / 2 - 0.5f);
            else
                startingX = pieceSize.x * (piecesCountX / 2);


            float startingY;
            if(piecesCountY % 2 == 0)
                startingY = pieceSize.y * (piecesCountY / 2 - 0.5f);
            else
                startingY = pieceSize.y * (piecesCountY / 2);


            return holder.transform.position - new Vector3(startingX, startingY, 0);
        }

        private Vector2 GetRandomInScreenPosition()
        {
            return new Vector2(Random.Range(-ScreenLimitX, ScreenLimitX), Random.Range(-ScreenLimitY, ScreenLimitY));
        }
    }
}
