﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAudioManager : MonoBehaviour
{
    public AudioManager audioManager;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) == true)
            audioManager.IsStopped = !audioManager.IsStopped;

        else if (Input.GetKeyDown(KeyCode.N) == true && audioManager.IsStopped == false)
            audioManager.PlayNext();
    }

}
