## TODO sąrašiukas
* Laurynai - sukurt paleidimo ikoną, žaidimo išjungimo, muzikos on/off, sfx on/off, laiko ir ėjimų ikonas.
* Aurimai - L3 ataskaita, "Apie komandą" skyrius pagrindiniame menu.
* Algirdai - Wiki tvarkymas.
* Žygimantai - reklaminis žaidimo pristatymas.


## Keletas minčių, "motyvacinių"
Mes ne kompanija - už klaidas iš darbo neatleis. Tačiau skola gali likt visai komandai. Visiems bus geriau, jei vieni kitus gerbsim ir laiku atliksim turimus darbus.  
Visi esam lygūs, jei turit pasiūlymų ar norit kažką pakeist - sakykit drąsiai.


## Naudojami įrankiai
Žaidimū variklis *Unity*:  
https://store.unity.com/download?ref=personal

Version Control klientas *SourceTree*:  
https://www.sourcetreeapp.com/


## Taisyklės
1. Dirbat tik savo branch'uose {*kiek kartų projektas žlugo dėl failų konfliktų...*}
2. Atliekat priskirtas užduotis pagal *Issue Tracker* (https://bitbucket.org/puzlekai/kur-buves-kur-nebuves/issues)
3. Jei priskirtų užduočių nebeliko - praneškit, rasim tikrai ką veikt 😇
4. Užduotis rinkitės pagal nutatytą prioritetą {*niekam neįdomios rašybos klaidos, kai neveikia pats mygtukas*}
5. NENUMESKIT PASKUTINEI DIENAI, BUS BLOGAI VISIEMS TADA... {*patirtis*}
6. Jei yra deadline'as - pasistenkit, kad iki jo viskas būtų padaryta